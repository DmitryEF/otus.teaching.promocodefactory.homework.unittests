﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Extensions
{
    using Microsoft.AspNetCore.Mvc;
    using Otus.Teaching.PromoCodeFactory.WebHost.Models;

    public static class SetPartnerPromoCodeLimitRequestExtensions
    {
        public static ObjectResult IsValid(this SetPartnerPromoCodeLimitRequest request)
        {
            if (request.Limit <= 0)
                return new BadRequestObjectResult("Лимит должен быть больше 0");

            return new OkObjectResult("Valid");
        }
    }
}

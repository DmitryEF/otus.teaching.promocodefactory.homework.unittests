﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Extensions
{
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
    using System.Linq;
    using System;
    using Microsoft.AspNetCore.Mvc;

    public static class PartnerExtensions
    {
        public static ObjectResult IsValid(this Partner partner)
        {
            if (partner == null)
                return new NotFoundObjectResult("Partner not found");

            //Если партнер заблокирован, то нужно выдать исключение
            if (!partner.IsActive)
                return new BadRequestObjectResult("Данный партнер не активен");

            return new OkObjectResult("Valid");
        }

        public static void DisableCurrentLimit(this Partner partner)
        {
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
                !x.CancelDate.HasValue);

            if (activeLimit == null) return;

            //Если партнеру выставляется лимит, то мы 
            //должны обнулить количество промокодов, которые партнер выдал, если лимит закончился, 
            //то количество не обнуляется
            partner.NumberIssuedPromoCodes = 0;

            //При установке лимита нужно отключить предыдущий лимит
            activeLimit.CancelDate = DateTime.Now;
        }
    }
}

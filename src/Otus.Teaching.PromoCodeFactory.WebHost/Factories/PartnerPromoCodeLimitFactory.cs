﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Factories
{
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
    using Otus.Teaching.PromoCodeFactory.WebHost.Models;
    using System;

    public static class PartnerPromoCodeLimitFactory
    {
        public static PartnerPromoCodeLimit Create(
            Partner partner, 
            SetPartnerPromoCodeLimitRequest request)
        {
            return new PartnerPromoCodeLimit
            {
                Limit = request.Limit,
                Partner = partner,
                PartnerId = partner.Id,
                CreateDate = DateTime.Now,
                EndDate = request.EndDate
            };
        }
    }
}

﻿namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoFixture;
    using FluentAssertions;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Moq;
    using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
    using Otus.Teaching.PromoCodeFactory.DataAccess;
    using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
    using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
    using Otus.Teaching.PromoCodeFactory.UnitTests.Factories;
    using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
    using Xunit;

    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Fixture fixture;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            this.fixture = new Fixture();
        }

        // 1
        [Fact]
        public async Task Partner_NotFound_ShouldReturns404()
        {
            var mockedRepository = new Mock<IRepository<Partner>>();

            var request = new SetPartnerPromoCodeLimitRequestBuilder().Create();

            mockedRepository
                .Setup(r => r.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => null);

            var controller = ControllersFactory.CreatePartnersController(mockedRepository.Object);

            var result = 
                await controller.SetPartnerPromoCodeLimitAsync(
                    Guid.NewGuid(), request) as NotFoundObjectResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be(404);
        }

        // 2
        [Fact]
        public async Task Partner_NotActive_ShouldReturns400()
        {
            var partner = new PartnerBuilder().WithActiveFalse().Create();

            var request = new SetPartnerPromoCodeLimitRequestBuilder().Create();

            var mockedRepository = new Mock<IRepository<Partner>>();

            mockedRepository
                .Setup(r => r.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => partner);

            var controller = ControllersFactory.CreatePartnersController(mockedRepository.Object);

            var result = await controller.SetPartnerPromoCodeLimitAsync(
                Guid.NewGuid(), request) as BadRequestObjectResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be(400);
        }

        // 3
        [Theory]
        [InlineData(false)]
        [InlineData(true)]
        public async Task Partner_IfGotOrNotPromoCodeLimit_ShouldResetOrNotNumberIssuedPromoCodes(bool hasCancelDate)
        {
            var partnerPromoCodeLimit = hasCancelDate
                ? new PartnerPromoCodeLimitBuilder().WithCanelDate().Create()
                : new PartnerPromoCodeLimitBuilder().WithoutCanelDate().Create();

            var partner = new PartnerBuilder()
                .WithActiveTrue()
                .WithNumberIssuedPromoCodes(1)
                .AddLimit(partnerPromoCodeLimit)
                .Create();

            var setPartnerPromoCodeLimitRequest = 
                new SetPartnerPromoCodeLimitRequestBuilder()
                    .WithLimit(1)
                    .WithEndDate(DateTime.Now.AddDays(1))
                    .Create();

            var mockedRepository = new Mock<IRepository<Partner>>();
            
            mockedRepository
                .Setup(r => r.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => partner);

            var controller = ControllersFactory.CreatePartnersController(mockedRepository.Object);

            await controller.SetPartnerPromoCodeLimitAsync(
                Guid.NewGuid(), setPartnerPromoCodeLimitRequest);

            partner.NumberIssuedPromoCodes.Should().Be(hasCancelDate ? 1 : 0);
        }

        // 4
        [Fact]
        public async Task Partner_IfGotPromoCodeLimit_ShouldDisablePreviousLimit()
        {
            var partnerPromoCodeLimit = 
                new PartnerPromoCodeLimitBuilder()
                    .WithoutCanelDate()
                    .Create();

            var partner = new PartnerBuilder()
                .WithActiveTrue()
                .WithNumberIssuedPromoCodes(1)
                .AddLimit(partnerPromoCodeLimit)
                .Create();

            var request =
                new SetPartnerPromoCodeLimitRequestBuilder()
                    .WithLimit(1)
                    .WithEndDate(DateTime.Now.AddDays(1))
                    .Create();

            var mockedRepository = new Mock<IRepository<Partner>>();

            mockedRepository
                .Setup(r => r.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => partner);

            var controller = ControllersFactory.CreatePartnersController(mockedRepository.Object);

            await controller.SetPartnerPromoCodeLimitAsync(
                Guid.NewGuid(), request);

            partnerPromoCodeLimit.CancelDate.Should().NotBeNull();
        }

        //5
        [Fact]
        public async Task RequestLimit_LessThenOne_ShouldReturns400()
        {
            var partner = new PartnerBuilder()
                .WithActiveTrue()
                .WithNumberIssuedPromoCodes(1)
                .WithPartnerLimits()
                .Create();

            var setPartnerPromoCodeLimitRequest =
                new SetPartnerPromoCodeLimitRequestBuilder()
                    .WithLimit(0)
                    .WithEndDate(DateTime.Now.AddDays(1))
                    .Create();

            var mockedRepository = new Mock<IRepository<Partner>>();

            mockedRepository
                .Setup(r => r.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => partner);

            var controller = new PartnersController(mockedRepository.Object);

            var result = await controller.SetPartnerPromoCodeLimitAsync(
                Guid.NewGuid(), 
                setPartnerPromoCodeLimitRequest) as BadRequestObjectResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be(400);
        }

        // 6
        [Fact]
        public async Task Partner_IfGotPromoCodeLimit_EnsureSaveChangesWasCalled()
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: "PromoCodeFactory")
                .Options;

            await using var context = new DataContext(options);

            var limit =
                new PartnerPromoCodeLimitBuilder()
                    .WithoutCanelDate()
                    .Create();

            var partner = new PartnerBuilder()
                .WithRandomId()
                .WithActiveTrue()
                .WithNumberIssuedPromoCodes(1)
                .AddLimit(limit)
                .Create();

            var request =
                new SetPartnerPromoCodeLimitRequestBuilder()
                    .WithLimit(1)
                    .WithEndDate(DateTime.Now.AddDays(1))
                    .Create();

            context.Partners.Add(partner);

            await context.SaveChangesAsync();

            var partnersRepository = new EfRepository<Partner>(context);

            var controller = new PartnersController(partnersRepository);

            await controller.SetPartnerPromoCodeLimitAsync(
                    partner.Id, request);

            var partnerFromDb = await context.Partners.FirstOrDefaultAsync(p => p.Id == partner.Id);

            partner.Should().NotBeNull();

            partnerFromDb.PartnerLimits.Should().NotBeNull();

            partnerFromDb.PartnerLimits.Count.Should().Be(2);

            partnerFromDb.PartnerLimits.Count(l => l.CancelDate.HasValue).Should().Be(1);
        }
    }
}
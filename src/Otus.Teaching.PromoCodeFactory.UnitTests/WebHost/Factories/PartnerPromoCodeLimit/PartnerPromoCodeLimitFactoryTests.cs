﻿namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Factories.PartnerPromoCodeLimit
{
    using System;
    using FluentAssertions;
    using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
    using Otus.Teaching.PromoCodeFactory.WebHost.Factories;
    using Xunit;

    public class PartnerPromoCodeLimitFactoryTests
    {
        [Fact]
        public void PartnerPromoCodeLimitFactory_WhenCreatingLimit_ShouldInitProperties()
        {
            var partner = new PartnerBuilder()
                .WithRandomId()
                .Create();

            var request =
                new SetPartnerPromoCodeLimitRequestBuilder()
                    .WithEndDate(DateTime.Now)
                    .WithLimit(5)
                    .Create();

            var limit = PartnerPromoCodeLimitFactory.Create(partner, request);

            limit.Limit.Should().Be(request.Limit);
            limit.EndDate.Should().Be(request.EndDate);
            limit.Partner.Should().Be(partner);
            limit.PartnerId.Should().Be(partner.Id);
            limit.CreateDate.TimeOfDay.Should().BeGreaterThan(DateTime.Now.AddSeconds(-10).TimeOfDay);
        }
    }
}

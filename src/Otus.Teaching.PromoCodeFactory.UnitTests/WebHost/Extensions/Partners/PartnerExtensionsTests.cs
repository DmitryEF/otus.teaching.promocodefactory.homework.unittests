﻿namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Extensions.Partners
{
    using FluentAssertions;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
    using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
    using Otus.Teaching.PromoCodeFactory.WebHost.Extensions;
    using Xunit;

    public class PartnerExtensionsTests
    {
        [Fact]
        public void Partner_WhenDisablingLimit_ShouldSetCancelDate()
        {
            var limit = new PartnerPromoCodeLimitBuilder()
                .WithoutCanelDate()
                .Create();

            var partner = new PartnerBuilder()
                .AddLimit(limit)
                .Create();

            partner.DisableCurrentLimit();

            limit.CancelDate.Should().NotBeNull();
        }

        [Fact]
        public void Partner_WhenDisablingLimit_ShouldResetNumberIssuedPromoCodes()
        {
            var limit = new PartnerPromoCodeLimitBuilder()
                .WithoutCanelDate()
                .Create();

            var partner = new PartnerBuilder()
                .WithNumberIssuedPromoCodes(5)
                .AddLimit(limit)
                .Create();

            partner.DisableCurrentLimit();

            partner.NumberIssuedPromoCodes.Should().Be(0);
        }
        
        [Fact]
        public void Partner_WhenDisablingCanceledLimit_ShouldNotResetNumberIssuedPromoCodes()
        {
            var limit = new PartnerPromoCodeLimitBuilder()
                .WithCanelDate()
                .Create();

            var partner = new PartnerBuilder()
                .WithNumberIssuedPromoCodes(5)
                .AddLimit(limit)
                .Create();

            partner.DisableCurrentLimit();

            partner.NumberIssuedPromoCodes.Should().Be(5);
        }

        [Fact]
        public void Partner_WhenValidateInactive_ShouldReturn400()
        {
            var limit = new PartnerPromoCodeLimitBuilder()
                .WithoutCanelDate()
                .Create();

            var partner = new PartnerBuilder()
                .WithNumberIssuedPromoCodes(5)
                .WithActiveFalse()
                .AddLimit(limit)
                .Create();

            var result = partner.IsValid();

            result.StatusCode.Should().Be(400);
        }

        [Fact]
        public void Partner_WhenValidateNull_ShouldReturn404()
        {
            Partner partner = null;

            var result = partner.IsValid();

            result.StatusCode.Should().Be(404);
        }

        [Fact]
        public void Partner_WhenValidationSuccess_ShouldReturn200()
        {
            var partner = new PartnerBuilder()
                .WithActiveTrue()
                .Create();

            var result = partner.IsValid();

            result.StatusCode.Should().Be(200);
        }
    }
}

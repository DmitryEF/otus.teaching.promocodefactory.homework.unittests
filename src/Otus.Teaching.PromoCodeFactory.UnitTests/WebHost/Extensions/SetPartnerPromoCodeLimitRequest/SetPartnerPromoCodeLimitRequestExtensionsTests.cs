﻿namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Extensions.SetPartnerPromoCodeLimitRequest
{
    using FluentAssertions;
    using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
    using Otus.Teaching.PromoCodeFactory.WebHost.Extensions;
    using Xunit;

    public class SetPartnerPromoCodeLimitRequestExtensionsTests
    {
        [Fact]
        public void SetPartnerPromoCodeLimitRequest_WhenValidateSuccess_ShouldReturn200()
        {
            var setPartnerPromoCodeLimitRequest =
                new SetPartnerPromoCodeLimitRequestBuilder()
                    .WithLimit(1)
                    .Create();

            var result = setPartnerPromoCodeLimitRequest.IsValid();

            result.StatusCode.Should().Be(200);
        }

        [Fact]
        public void SetPartnerPromoCodeLimitRequest_WhenValidateWrongLimit_ShouldReturn404()
        {
            var setPartnerPromoCodeLimitRequest =
                new SetPartnerPromoCodeLimitRequestBuilder()
                    .WithLimit(0)
                    .Create();

            var result = setPartnerPromoCodeLimitRequest.IsValid();

            result.StatusCode.Should().Be(400);
        }
    }
}

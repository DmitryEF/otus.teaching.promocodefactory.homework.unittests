﻿namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    using System;
    using Otus.Teaching.PromoCodeFactory.WebHost.Models;

    public class SetPartnerPromoCodeLimitRequestBuilder
    {
        private int limit;
        private DateTime endDate;

        public SetPartnerPromoCodeLimitRequestBuilder WithLimit(int limit)
        {
            this.limit = limit;

            return this;
        }

        public SetPartnerPromoCodeLimitRequestBuilder WithEndDate(DateTime endDateTime)
        {
            this.endDate = endDateTime;

            return this;
        }

        public SetPartnerPromoCodeLimitRequest Create()
        {
            return new SetPartnerPromoCodeLimitRequest
            {
                Limit = this.limit,
                EndDate = this.endDate
            };
        }
    }
}

﻿namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    using System;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

    public class PartnerPromoCodeLimitBuilder
    {
        private DateTime? cancelDate;

        public PartnerPromoCodeLimitBuilder WithCanelDate()
        {
            this.cancelDate = DateTime.Now;

            return this;
        }

        public PartnerPromoCodeLimitBuilder WithoutCanelDate()
        {
            this.cancelDate = null;

            return this;
        }

        public PartnerPromoCodeLimit Create()
        {
            return new PartnerPromoCodeLimit
            {
                CancelDate = this.cancelDate
            };
        }
    }
}

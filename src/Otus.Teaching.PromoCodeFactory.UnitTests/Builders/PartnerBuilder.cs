﻿namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    using System;
    using System.Collections.Generic;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

    public class PartnerBuilder
    {
        private Guid id;
        private bool isActive;
        private string name;
        private int numberIssuedPromoCodes;
        private ICollection<PartnerPromoCodeLimit> partnerLimits;

        public PartnerBuilder WithRandomId()
        {
            this.id = Guid.NewGuid();

            return this;
        }

        public PartnerBuilder WithActiveTrue()
        {
            this.isActive = true;

            return this;
        }

        public PartnerBuilder WithActiveFalse()
        {
            this.isActive = false;

            return this;
        }
        
        public PartnerBuilder WithName(string partnerName)
        {
            this.name = partnerName;

            return this;
        }

        public PartnerBuilder WithNumberIssuedPromoCodes(int count)
        {
            this.numberIssuedPromoCodes = count;

            return this;
        }

        public PartnerBuilder WithPartnerLimits()
        {
            this.partnerLimits = new List<PartnerPromoCodeLimit>();

            return this;
        }

        public PartnerBuilder AddLimit(PartnerPromoCodeLimit partnerPromoCodeLimit)
        {
            this.partnerLimits ??= new List<PartnerPromoCodeLimit>();

            this.partnerLimits.Add(partnerPromoCodeLimit);

            return this;
        }

        public Partner Create()
        {
            return new Partner
            {
                Id = this.id,
                IsActive = this.isActive,
                Name = this.name,
                NumberIssuedPromoCodes = this.numberIssuedPromoCodes,
                PartnerLimits = this.partnerLimits
            };
        }
    }
}

﻿namespace Otus.Teaching.PromoCodeFactory.UnitTests.Factories
{
    using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
    using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

    public static class ControllersFactory
    {
        public static PartnersController CreatePartnersController(IRepository<Partner> partnerRepository)
        {
            return new PartnersController(partnerRepository);
        }
    }
}
